export const baseUrl = 'http://3.12.158.241/Ci_project/api/';

export const otherLinks = {
  register: baseUrl + 'Authentication/registration',
  login: baseUrl + 'Authentication/login',
  createTask: baseUrl + 'createTask',
  getalltask: baseUrl + 'tasks/getalltask',
  getAllUnassignTasks: baseUrl + '/getAllUnassignTasks',
  getSingletask: baseUrl + 'tasks/gettask',
  updateUser: baseUrl + 'updateUser',
  getAllUser: baseUrl + 'getAllUser',
  assigntask: baseUrl + 'assigntask',
  addComments: baseUrl + 'addComments',
  getComments: baseUrl + 'getComments',
  unassign: baseUrl + 'unassign',
  searchtask: baseUrl + 'searchtask',
};
