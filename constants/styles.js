import {StyleSheet} from 'react-native';
import {Dimensions} from 'react-native';
export default StyleSheet.create({
  container: {
    flexDirection: 'column',
    backgroundColor: '#fff',
  },
  wrapper: {
    padding: 20,
  },
  Headingtext: {
    textAlign: 'center',
    width: '100%',
    fontSize: 50,
    marginVertical: 100,
  },
  directionRow: {
    flexDirection: 'row',
  },
  link: {
    color: 'blue',
  },
  centerContent: {
    justifyContent: 'center',
  },
  VerticalSpace: {
    width: '100%',
    height: 20,
  },
  buttonDesign: {
    paddingHorizontal: 50,
    paddingVertical: 10,
    width: '100%',
    backgroundColor: '#1f89dc',
    color: '#fff',
    borderRadius: 50,
  },
  cardWrap: {
    backgroundColor: '#ececec',
    width: '100%',
    borderRadius: 15,
  },
  cardImage: {
    width: '25%',
    height: 70,
    marginRight: '5%',
    borderRadius: 10,
  },
  rightConteCard: {
    paddingLeft: 50,
    justifyContent: 'space-between',
  },
  imageMyaccount: {
    width: Dimensions.get('window').width / 3,
    height: Dimensions.get('window').width / 3,
  },
  AccountformWrap: {
    paddingTop: 40,
    paddingBottom: 20,
    paddingHorizontal: 20,
    marginTop: 20,
    backgroundColor: '#f0f4f5',
    borderRadius: 20,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    width: '90%',
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  modalClose: {
    position: 'absolute',
    right: 0,
    backgroundColor: 'red',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 10,
  },
  resetButton:{
    backgroundColor: 'red',
    paddingVertical: 5,
    paddingHorizontal: 20,
    borderRadius: 10,
    height: 32,
    marginTop: 10,
  },
  resetButtonText:{
    color: 'white',
  },
  greyColor:{
    backgroundColor: '#ccc',
  },
  greyText:{
    color: '#000',
  }
});
