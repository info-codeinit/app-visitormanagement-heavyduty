import {CREATE_TASK, UPDATE_USER_AUTH, LISTING_TASKS} from '../actions/tasks';
const initialState = {
  userToken: '',
  email: '',
  userDetails: [],
  taskList: [],
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_TASK:
      return {
        ...state,
        username: action.data,
      };
    case UPDATE_USER_AUTH:
      return {
        ...state,
        userDetails: action.data,
        email: action.email,
      };
    case LISTING_TASKS:
      return {
        ...state,
        taskList: action.data,
      };
    default:
      return state;
  }
  //return state;
};

export default userReducer;
