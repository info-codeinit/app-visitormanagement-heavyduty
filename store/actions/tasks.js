export const CREATE_TASK = 'CREATE_TASK';
export const UPDATE_USER_AUTH = 'UPDATE_USER_AUTH';
export const LISTING_TASKS = 'LISTING_TASKS';
export const LOGOUT_USER = 'LOGOUT_USER';

export const createTaskaction = task_data => {
  return {
    type: CREATE_TASK,
    data: task_data,
  };
};

export const updateUserAuth = userInfo => {
  return {
    type: UPDATE_USER_AUTH,
    data: userInfo,
    email: userInfo.data.email,
  };
};

export const listingTaks = taskList => {
  return {
    type: LISTING_TASKS,
    data: taskList,
  };
};

export const logoutUser = userInfo => {
  return {
    type: LOGOUT_USER,
    email: null,
  };
};
