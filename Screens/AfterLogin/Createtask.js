import React, {useState} from 'react';
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
  Modal,
  Alert,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {Input} from 'react-native-elements/dist/input/Input';
import styles from '../../constants/styles';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {Icon} from 'react-native-elements';
import {otherLinks} from '../../constants/Links';
import {listingTaks} from '../../store/actions/tasks';

const Createtask = ({navigation}) => {

  /**
   * Redux State Constants
   */
  const ReduxTaskList = useDispatch();
  //ReduxTaskList(listingTaks({data: json.data}));
  const taskListRedux = useSelector(state => state.users);

  /**
   * Other Constants
   */
  let taskImage = require('../../constants/images/placeholderimage.jpg');
  const [imageSource, setImageSource] = useState(taskImage);
  const [imageBase64, setimageBase64] = useState();
  const [modalVisible, setModalVisible] = useState(false);
  const currentUserId = useSelector(state => state.users.userDetails.data.id);
  const [phone, setPhone] = useState('');
  const [visitorname, setVisitorname] = useState('');
  const [comment, setComment] = useState('');


  /**
   * Seleting Image to upload}
   * @param {image source camera/lib} sourceFrom
   */
  const selectImage = sourceFrom => {
    let options = {
      title: 'You can choose one image',
      maxWidth: 256,
      maxHeight: 256,
      noData: false,
      includeBase64: true,
      storageOptions: {
        skipBackup: true,
      },
    };
    setModalVisible(false);
    //alert(sourceFrom);

    if (sourceFrom == 'camera') {
      launchCamera(options, response => {
        console.log('image contetn', response.assets[0]);
        if (response) {
          console.log(response);
        }
        if (response.didCancel) {
          console.log('User cancelled photo picker');
          Alert.alert('Message', 'You did not select any image');
        } else if (response.error) {
          console.log('CODE Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
          //parent assets
          // fileName: "", fileSize: 123, height: 123, type: "", base63: ..., uri: "file path"

          let source = {uri: response.assets[0].uri};
          let imageBase64 = response.assets[0].base64;
          setImageSource(source);
          setimageBase64(imageBase64);
        }
      });
    } else {
      launchImageLibrary(options, response => {
        console.log('content for base 64', JSON.stringify(response));
        if (response) {
          console.log(response);
        }
        if (response.didCancel) {
          Alert.alert('Message', 'You did not select any image');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
          let source = {uri: response.assets[0].uri};
          let imageBase64 = response.assets[0].base64;
          setImageSource(source);
          setimageBase64(imageBase64);
        }
      });
    }
  };


  /**
   * Fetching all tasks from Server
   */
   const fetchTasklist = () => {
    fetch(otherLinks.getalltask, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        offset: 1,
      }),
    })
      .then(response => response.json())
      .then(jsonAllTasksData => {
        const jsonAllTasks = jsonAllTasksData.data;
        console.log('Returned Create Screen data: ', jsonAllTasks);
        ReduxTaskList(listingTaks({data: jsonAllTasks}));
        //console.log('redux data: ', taskListRedux);

      })
      .catch(error => {
        console.log('Error while loading Tasks: ', error);
      });
  };

  /**
   * Creating Task
   */
  const creatingTaksAPI = () => {
    if (visitorname == '' || phone == '' || !visitorname || !phone) {
      Alert.alert('Error', 'Please fill the required fields');
      return false;
    }

    let formData = new FormData();

    if (imageBase64) {
      formData.append('image', imageBase64);
    }
    formData.append('phone_number', phone);
    formData.append('comment', comment);
    formData.append('visitor_name', visitorname);
    formData.append('user_id', currentUserId);

    console.log('formData = ', formData);

    //{"_parts": [["image", "..."], ["phone_number", "09809809908"], ["comment", "happy comment"], ["visitor_name", "Prayas"], ["user_id", "29"]]}

    fetch(otherLinks.createTask, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      body: formData,
    })
      .then(response => response.json())
      .then(response => {
        console.log('On task Creation: ', response);
        Alert.alert('Message', 'A task has been created');
        setimageBase64(null);
        setPhone(null);
        setVisitorname(null);
        setComment(null);
        setImageSource(taskImage);
        fetchTasklist();
      })
      .catch(error => {
        console.log('You can not proceed', error);
      });
  };

  return (
    <ScrollView style={styles.container}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={[styles.modalView]}>
            <View
              style={[
                styles.directionRow,
                styles.centerContent,
                {justifyContent: 'space-around', width: '100%'},
              ]}>
              <Icon
                name="camera"
                type="font-awesome"
                color="#f50"
                size={70}
                onPress={() => selectImage('camera')}
              />
              <Icon
                name="picture-o"
                type="font-awesome"
                color="#f50"
                size={70}
                onPress={() => selectImage('gallery')}
              />
            </View>
            <TouchableOpacity
              style={styles.modalClose}
              onPress={() => setModalVisible(!modalVisible)}>
              <Text style={{color: 'white'}}>Close</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
      <View style={styles.wrapper}>
        <Text style={[styles.Headingtext, {marginVertical: 20}]}>
          Create Task
        </Text>
        <View style={styles.formWrapper}>
          <TouchableOpacity
            style={[
              styles.directionRow,
              styles.centerContent,
              {paddingVertical: 40},
            ]}
            onPress={() => setModalVisible(!modalVisible)}>
            <Image style={{width: 100, height: 100}} source={imageSource} />
          </TouchableOpacity>

          <Input
            placeholder="Name"
            label="Name"
            leftIcon={{type: 'font-awesome', name: 'user'}}
            value={visitorname}
            onChangeText={visitorname => setVisitorname(visitorname)}
          />
          <Input
            placeholder="Phone number"
            label="Phone number"
            leftIcon={{type: 'font-awesome', name: 'phone'}}
            value={phone}
            onChangeText={phone => setPhone(phone)}
          />

          <Input
            leftIcon={{type: 'font-awesome', name: 'comment'}}
            label="Comment"
            placeholder="Comment"
            multiline={true}
            numberOfLines={2}
            value={comment}
            onChangeText={comment => setComment(comment)}
          />

          <View style={[styles.centerContent, styles.directionRow]}>
            <TouchableOpacity onPress={() => creatingTaksAPI()}>
              <Text style={styles.buttonDesign}>Create Task</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default Createtask;
