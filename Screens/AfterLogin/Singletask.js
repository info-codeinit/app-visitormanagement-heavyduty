import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  Linking,
  FlatList,
} from 'react-native';
import {Icon, Input} from 'react-native-elements';
import styles from '../../constants/styles';
import {otherLinks} from '../../constants/Links';
import {useSelector} from 'react-redux';

const SingleTask = ({navigation, route}) => {
  const currentUserId = useSelector(state => state.users.userDetails.data.id);
  const cuttentTask = route.params.taskId;
  const taskName = route.params.taskName;
  const taskComment = route.params.taskComment;
  const taskPhone = route.params.taskPhone;
  const taskCreated = route.params.taskCreated;
  const taskImage = route.params.taskImage;
  const [dataAllComments, setdataAllComments] = useState(null);
  const [addComment, setddComment] = useState(null);
  //alert(taskImage);

  useEffect(() => {
    fetchCommentlist();
  }, []);

  /***
   * Creting Task
   */
  const createComment = () => {
    fetch(otherLinks.addComments, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        comment: addComment,
        task_id: cuttentTask,
        user_id: currentUserId,
      }),
    })
      .then(response => response.json())
      .then(response => {
        if (response.message) {
          alert('Comment has been added');
          setddComment(null);
          fetchCommentlist();
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  /***
   * Getting all comment for the current task
   */
  const fetchCommentlist = () => {
    //setLoading(true);
    fetch(otherLinks.getComments, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        task_id: cuttentTask,
      }),
    })
      .then(response => response.json())
      .then(responseComments => {
        console.log('this re commet response', responseComments);
        setdataAllComments(responseComments.data);
      })
      .catch(error => {
        console.log(error);
        //setLoading(false);
      });
  };

  const singleComment = ({item}) => {
    return (
      <View
        style={[
          styles.cardWrap,
          {flex: 1, paddingBottom: 5, marginBottom: 20},
        ]}>
        <View style={[styles.wrapper, {paddingBottom: 10}]}>
          <View style={[styles.rightContenCard]}>
            <View
              style={[styles.directionRow, {justifyContent: 'space-between'}]}>
              <Text>
                <Text style={{fontWeight: 'bold'}}>{item.name} </Text>
                <Text style={{paddingVertical: 5}}>({item.created})</Text>
              </Text>
              <View style={styles.directionRow}>
                <Icon
                  name="phone"
                  type="font-awesome"
                  color="#f50"
                  size={15}
                  onPress={() => dialCall(item.phone)}
                />
              </View>
            </View>
            <Text style={{paddingVertical: 8}}>{item.comment}</Text>
          </View>
        </View>
      </View>
    );
  };

  const dialCall = userNumber => {
    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${' + userNumber + '}';
    } else {
      phoneNumber = 'telprompt:${' + userNumber + '}';
    }

    Linking.openURL(phoneNumber);
  };
  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
        <FlatList
          ListHeaderComponent={
            <View>
              <View style={[styles.directionRow]}>
                {taskImage ? (
                  <Image
                    style={styles.cardImage}
                    source={{
                      uri: taskImage,
                    }}
                  />
                ) : (
                  <Image
                    style={{width: '25%', height: 100}}
                    source={require('../../constants/images/user.png')}
                  />
                )}
                <View style={{width: '72%', marginLeft: '3%'}}>
                  <Text>Name: {taskName} </Text>
                  <Text>Creted On: ({taskCreated})</Text>
                  <Text>Phone: {taskPhone}</Text>
                  <Text>
                    Comment: {taskComment ? taskComment : 'No Comment'}
                  </Text>
                </View>
              </View>

              <View>
                <Input
                  placeholder="Comment"
                  value={addComment}
                  onChangeText={addComment => setddComment(addComment)}
                  multiline={true}
                />
                <View style={[styles.centerContent, styles.directionRow]}>
                  <TouchableOpacity
                    style={{width: '100%'}}
                    onPress={() => createComment()}>
                    <Text style={[styles.buttonDesign, {textAlign: 'center'}]}>
                      Add Comment
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.VerticalSpace} />
            </View>
          }
          data={dataAllComments}
          renderItem={singleComment}
          keyExtractor={item => item.id}
          style={{paddingBottom: 200}}
        />
      </View>
    </View>
  );
};

export default SingleTask;
