import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  Linking,
  Platform,
  Alert,
} from 'react-native';
import {Icon, Divider, Input} from 'react-native-elements';
import styles from '../../constants/styles';
import {otherLinks} from '../../constants/Links';
import {useIsFocused} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {listingTaks} from '../../store/actions/tasks';
import {SafeAreaView} from 'react-native-safe-area-context';

const Tasklist = ({navigation}) => {
  const ReduxTaskList = useDispatch();
  const taskListRedux = useSelector(state => state.users.taskList);

  const isFocused = useIsFocused();
  const [dataAllTasks, setdataAllTasks] = useState();
  const [paginate, setPaginate] = useState(1);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [allowLoadMore, setAllowLoadMore] = useState(true);
  const [searching, setSearching] = useState(false);
  const [searchText, setSearch] = useState();

  useEffect(() => {
    fetchTasklist();
  }, []);

  const paginateListing = () => {
    console.log(allowLoadMore);
    if (!allowLoadMore) {
      return false;
    }
    loadMore();
  };

  const refreshListing = () => {
    setIsRefreshing(true);
    setPaginate(1);
    setSearching(false);
    setAllowLoadMore(true);
    fetchTasklist();
  };

  /**
   * Fetching Tasks all tasks from Server
   */
  const fetchTasklist = () => {
    ReduxTaskList(listingTaks({data: []}));
    fetch(otherLinks.getalltask, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        offset: 1,
      }),
    })
      .then(response => response.json())
      .then(jsonAllTasksData => {
        let jsonAllTasks = jsonAllTasksData.data;

        console.log('Returned data Teasklisk.js: ', jsonAllTasks);
        ReduxTaskList(listingTaks({data: jsonAllTasks}));

        if (jsonAllTasks.length == 0) {
          setAllowLoadMore(false);
          Alert.alert('Message', "You Don't have any assigned tasks");
          return false;
        }
        //alert('Updating Content');
        console.log('Redux data Teasklisk.js: ', taskListRedux.data);
        setPaginate(2);
        setIsRefreshing(false);
      })
      .catch(error => {
        console.log('Error while loading Tasks: ', error);
        setIsRefreshing(false);
      });
  };

  /**
   * LoadingMore all tasks from Server
   */
  const loadMore = () => {
    if (searching || !allowLoadMore) {
      return false;
    }
    fetch(otherLinks.getalltask, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        offset: paginate,
      }),
    })
      .then(response => response.json())
      .then(jsonAllTasksData => {
        const jsonAllTasks = jsonAllTasksData.data;
        console.log('Returned data: ', jsonAllTasks);
        if (jsonAllTasks.length == 0) {
          setAllowLoadMore(false);
          //Alert.alert('No more tasks available');
          console.log('No more tasks available');
          //setPaginate(1);
          return false;
        }
        let tempCurrentData = taskListRedux.data;
        jsonAllTasks.map(itemTask => tempCurrentData.push(itemTask));
        //setdataAllTasks(tempCurrentData);
        ReduxTaskList(listingTaks({data: tempCurrentData}));
        setPaginate(paginate + 1);
        setIsRefreshing(false);
      })
      .catch(error => {
        console.log('Error while loading Tasks: ', error);
        setIsRefreshing(false);
      });
  };

  const singleTaskCard = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('Singletask', {
            taskId: item.id,
            taskName: item.visitor_name,
            taskComment: item.comment,
            taskImage: item.image,
            taskCreated: item.created,
            taskPhone: item.phone_number,
          })
        }
        style={[styles.cardWrap, {paddingBottom: 5, marginBottom: 20}]}>
        <View
          style={[styles.directionRow, styles.wrapper, {paddingBottom: 10}]}>
          {item.image ? (
            <Image
              style={styles.cardImage}
              source={{
                uri: item.image,
              }}
            />
          ) : (
            <Image
              style={styles.cardImage}
              source={require('../../constants/images/user.png')}
            />
          )}
          <View style={[styles.rightContenCard, {width: '70%'}]}>
            <View
              style={[styles.directionRow, {justifyContent: 'space-between'}]}>
              <Text>
                {item.visitor_name} ({item.id})
              </Text>
              <View style={styles.directionRow}>
                <Icon
                  name="phone"
                  type="font-awesome"
                  color="#f50"
                  size={15}
                  onPress={() => dialCall(item.phone_number)}
                />
              </View>
            </View>
            <Text>
              <Text style={{fontWeight: 'bold'}}>Comment:</Text>
              <Text>
                {' '}
                {item.comment.length > 60
                  ? item.comment.substring(0, 60) + '...'
                  : item.comment}
              </Text>
            </Text>
          </View>
          <View style={styles.VerticalSpace} />
        </View>
        <Divider orientation="horizontal" subHeaderStyle={{color: 'blue'}} />
        <View style={[styles.directionRow, {justifyContent: 'space-between'}]}>
          <Text style={{paddingLeft: 20, paddingVertical: 5}}>
            {item.created}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  /**
   *
   * @param {Calling number from dialer} userNumber
   */
  const dialCall = userNumber => {
    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${' + userNumber + '}';
    } else {
      phoneNumber = 'telprompt:${' + userNumber + '}';
    }

    Linking.openURL(phoneNumber);
  };

  /**
   *
   * @returns Results from Search API
   */
  const searchTask = () => {
    setdataAllTasks(false);
    setPaginate(1);
    setSearching(true);
    if (!searchText) {
      Alert.alert('Message', 'Search is empty');
      return false;
    }
    fetch(otherLinks.searchtask, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        offset: 1,
        search_field: searchText,
      }),
    })
      .then(response => response.json())
      .then(jsonAllTasksData => {
        const jsonSearchedTasks = jsonAllTasksData.data;
        if (jsonSearchedTasks.length == 0) {
          setAllowLoadMore(false);
          Alert.alert('Message', 'No tasks available');
          return false;
        }
        if (paginate > 1) {
          //alert(paginate);
          //jsonSearchedTasks.map(itemTask => dataAllTasks.push(itemTask));
        }
        ReduxTaskList(listingTaks({data: jsonSearchedTasks}));
        console.log('Searched tasks: ', jsonSearchedTasks);

        setIsRefreshing(false);
        setSearching(true);
      })
      .catch(error => {
        console.log('Error while loading Tasks: ', error);
        setIsRefreshing(false);
      });
  };
  const resetSearch = () => {
    setSearching(false);
    setSearch('');
    fetchTasklist();
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={[styles.wrapper]}>
        {taskListRedux.data ? (
          <FlatList
            ListHeaderComponent={
              <View style={[styles.directionRow, {width: '80%'}]}>
                <Input
                  placeholder="Search"
                  value={searchText}
                  rightIcon={{
                    type: 'ionicon',
                    name: 'search',
                    onPress: searchTask,
                  }}
                  onChangeText={searchTexthere => setSearch(searchTexthere)}
                />
                {searching ? (
                  <TouchableOpacity
                    style={styles.resetButton}
                    onPress={() => resetSearch()}>
                    <Text style={styles.resetButtonText}>Reset</Text>
                  </TouchableOpacity>
                ) : (
                  <View style={[styles.resetButton, styles.greyColor]}>
                    <Text style={[styles.resetButtonText, styles.greyText]}>Reset</Text>
                  </View>
                )}
              </View>
            }
            data={taskListRedux.data}
            renderItem={singleTaskCard}
            keyExtractor={item => item.id}
            refreshing={isRefreshing}
            onRefresh={refreshListing}
            onEndReached={paginateListing}
            onEndThreshold={0}
          />
        ) : (
          <View style={styles.directionRow}>
            <Text>Loading tasks...</Text>
          </View>
        )}
      </View>
    </SafeAreaView>
  );
};

export default Tasklist;
