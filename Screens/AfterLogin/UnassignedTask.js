import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  Modal,
  FlatList,
  Linking,
  Platform,
} from 'react-native';
import {Icon, Divider, Button} from 'react-native-elements';
import styles from '../../constants/styles';
import DropDownPicker from 'react-native-dropdown-picker';
import {otherLinks} from '../../constants/Links';
import {useSelector} from 'react-redux';
import {SafeAreaView} from 'react-native-safe-area-context';

const UnassignedTask = ({navigation}) => {
  const currentUserId = useSelector(state => state.users.userDetails.data.id);
  const [dataAllTasks, setdataAllTasks] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);
  const [offsetVal, setoffsetVal] = useState(1);
  const [assignTaskid, setAssignTaskid] = useState(false);
  const [open, setOpen] = useState(false);
  const [selectedUser, setValue] = useState(null);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [items, setItems] = useState([{label: 'Select User', value: '0'}]);

  const openAssignBox = (taskID) => {
    setAssignTaskid(taskID);
    setModalVisible(true);
  };
  const assignToUser = () => {
    fetch(otherLinks.assigntask, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        user_id: selectedUser,
        task_id: assignTaskid,
      }),
    })
      .then(response => response.json())
      .then(response => {
        alert('Task has been assigned');
        setModalVisible(false);
        fetchTasklist();
      })
      .catch(error => {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchTasklist();
    getUserslist();
  }, []);


  /**
   * Showing UNASSIGNED Tasks
   */
  const fetchTasklist = () => {
    setdataAllTasks(null);
    fetch(otherLinks.getAllUnassignTasks, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        offset: offsetVal,
      }),
    })
      .then(response => response.json())
      .then(jsonAllTasksData => {
        const jsonAllTasks = jsonAllTasksData.data;
        console.log('showing usassigned tasks', jsonAllTasks);
        setdataAllTasks(jsonAllTasks);
        setIsRefreshing(false);
      })
      .catch(error => {
        console.log(error);
        setIsRefreshing(false);
      });
  };

  const getUserslist = () => {
    let userArray = [];
    fetch(otherLinks.getAllUser)
      .then(response => response.json())
      .then(response => {
        const allUsers = response.data;
        //console.log('fetching all users: ', allUsers);
        allUsers.map(user =>
          userArray.push({label: user.email, value: user.id}),
        );
        //console.log('new user list', userArray);
        setItems(userArray);
        //setUsersList(allUsers);
      })
      .catch(error => {
        console.log(error);
      });
  };

  const singleTaskCard = ({item}) => {
    return (
      <View style={[styles.cardWrap, {backgroundColor: '#fff',paddingBottom: 5, marginBottom: 20}]}>
        <View
          style={[styles.directionRow, styles.wrapper, {paddingBottom: 10}]}>
          {item.image ? 
            <Image
              style={styles.cardImage}
              source={{
                uri: item.image,
              }}
            />
           : 
            <Image
              style={styles.cardImage}
              source={require('../../constants/images/user.png')}
            />
          }
          <View style={[styles.rightContenCard, {width: '70%'}]}>
            <View
              style={[styles.directionRow, {justifyContent: 'space-between'}]}>
              <Text>
                {item.visitor_name} ({item.id})
              </Text>
              <View style={styles.directionRow}>
                <Icon
                  name="phone"
                  type="font-awesome"
                  color="#f50"
                  size={15}
                  onPress={() => dialCall(item.phone_number)}
                />
              </View>
            </View>
            <Text>
              <Text style={{fontWeight: 'bold'}}>Comment:</Text>
              <Text>
                {' '}
                {item.comment.length > 60
                  ? item.comment.substring(0, 60) + '...'
                  : item.comment}
              </Text>
            </Text>
          </View>
        </View>
        <Divider orientation="horizontal" subHeaderStyle={{color: 'blue'}} />
        <View style={[styles.directionRow, {justifyContent: 'space-between'}]}>
          <Text style={{paddingLeft: 20, paddingVertical: 5}}>
            {item.created}
          </Text>
          <TouchableOpacity onPress={() => openAssignBox(item.id)}>
            <Text
              style={{
                color: 'white',
                backgroundColor: 'red',
                paddingVertical: 2,
                paddingHorizontal: 12,
                borderRadius: 10,
                marginTop: 3,
                marginRight: 20,
              }}>
              Assign to:
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };
  const dialCall = phone => {
    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${' + phone + '}';
    } else {
      phoneNumber = 'telprompt:${' + phone + '}';
    }

    Linking.openURL(phoneNumber);
  };

  return (
    <SafeAreaView style={[styles.container, {backgroundColor: '#ffe9e9'}]}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={[styles.modalView]}>
            <View
              style={[styles.centerContent, {width: '100%', minHeight: 500}]}>
              <Text>Select the user to assign this task</Text>
              <View style={styles.VerticalSpace} />
              <DropDownPicker
                open={open}
                value={selectedUser}
                items={items}
                setOpen={setOpen}
                setValue={setValue}
                setItems={setItems}
              />
              <View style={styles.VerticalSpace} />
              <View style={[styles.centerContent, styles.directionRow]}>
                <TouchableOpacity onPress={() => assignToUser(selectedUser)}>
                  <Text style={[styles.buttonDesign]}>Assign</Text>
                </TouchableOpacity>
              </View>
            </View>
            <TouchableOpacity
              style={styles.modalClose}
              onPress={() => setModalVisible(!modalVisible)}>
              <Text style={{color: 'white'}}>Close</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>

      <View style={styles.wrapper}>
        {dataAllTasks ? (
          <FlatList
            data={dataAllTasks}
            renderItem={singleTaskCard}
            keyExtractor={item => item.id}
            style={{paddingBottom: 200}}
            refreshing={isRefreshing}
            onRefresh={fetchTasklist}
          />
        ) : (
          <Text>Loading unassigned tasks</Text>
        )}
      </View>
    </SafeAreaView>
  );
};

export default UnassignedTask;
