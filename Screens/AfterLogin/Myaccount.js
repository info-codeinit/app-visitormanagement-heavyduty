import React, {useState} from 'react';
import {Text, View, ScrollView, TouchableOpacity, Image} from 'react-native';
import {Input} from 'react-native-elements';
import styles from '../../constants/styles';
import {useDispatch, useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import {logoutUser} from '../../store/actions/tasks';
import {otherLinks} from '../../constants/Links';
import {SafeAreaView} from 'react-native-safe-area-context';

const Myaccount = () => {
  const deleteUsernLogout = useDispatch();
  const contentredux = useSelector(state => state.users);
  const logOut = () => {
    deleteUsernLogout(logoutUser({email: ''}));
    console.log(contentredux);
  };
  const updateUser = async () => {

    if (!newphone) {
      alert('Please enter phone');
      return false;
    }

    const data = {
      name: name,
      phone: newphone,
      user_id: userid,
    };
    console.log('Sending this: ', data, otherLinks.updateUser);
    try {
      const response = await fetch(otherLinks.updateUser, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });
      const json = await response.json();
      console.log('return Content userUpdate: ', json);
      if (json.message == 'The user has been updated successfully.') {
        alert('Successfully Updated');
      }
    } catch (error) {
      console.error(error);
    } finally {
      //setLoading(false);
    }
  };
  const username = useSelector(state => state.users.userDetails.data.name);
  const useremail = useSelector(state => state.users.userDetails.data.email);
  const userphone = useSelector(state => state.users.userDetails.data.phone);
  const userid = useSelector(state => state.users.userDetails.data.id);
  const [name, setName] = useState(username);
  const [newphone, setPhone] = useState(userphone);
  return (
    <SafeAreaView style={{flex:1, flexDirection: 'column'}}>
    <ScrollView style={styles.container}>
      <View style={styles.wrapper}>
        <View style={[styles.centerContent, styles.directionRow]}>
          <Image
            style={[styles.cardImage, styles.imageMyaccount]}
            source={require('../../constants/images/user.png')}
          />
        </View>
        <View style={[styles.AccountformWrap]}>
          <View>
            <Input
              placeholder="Name"
              leftIcon={{type: 'font-awesome', name: 'user'}}
              value={name}
              onChangeText={name => setName(name)}
              label="Name"
            />
          </View>
          <View>
            <Input
              placeholder="email"
              leftIcon={{type: 'font-awesome', name: 'envelope'}}
              value={useremail}
              disabled={true}
              label="Email"
            />
          </View>
          <View>
            <Input
              placeholder="Phone"
              leftIcon={{type: 'font-awesome', name: 'phone'}}
              value={newphone}
              onChangeText={newphone => setPhone(newphone)}
              label="Phone"
            />
          </View>
          <View style={[styles.centerContent, styles.directionRow]}>
            <TouchableOpacity onPress={() => updateUser()}>
              <Text style={styles.buttonDesign}>Update</Text>
            </TouchableOpacity>
          </View>

          <View
            style={[
              {justifyContent: 'flex-end', marginTop: 20},
              styles.directionRow,
            ]}>
            <TouchableOpacity
              style={[styles.directionRow]}
              onPress={() => logOut()}>
              <Icon
                name="sign-out"
                color="#f50"
                size={20}
                onPress={() => alert('hi')}
              />
              <Text>Log out</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
    </SafeAreaView>
  );
};

export default Myaccount;
