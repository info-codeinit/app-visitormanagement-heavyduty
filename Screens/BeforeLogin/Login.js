import React, {useState, useEffect} from 'react';
import {Text, View, ScrollView, TouchableOpacity} from 'react-native';
import {Input} from 'react-native-elements/dist/input/Input';
import styles from '../../constants/styles';
import {useDispatch, useSelector} from 'react-redux';
import {updateUserAuth} from '../../store/actions/tasks';
import {otherLinks} from '../../constants/Links';
import messaging from '@react-native-firebase/messaging';
import {Alert} from 'react-native';

const Login = ({navigation}) => {

  const [fcmToken, setFCMToken] = useState();
  const requestUserPermission = async () => {
    const Token = await messaging().getToken();
    setFCMToken(Token);
    console.log('token1: ', Token);
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
    });

    return unsubscribe;
  };

  useEffect(() => {
    requestUserPermission();
  }, []);

  const changeloginStatus = useDispatch();
  const showPassword = () => {
    showPass ? setshowPass(false) : setshowPass(true);
  };
  const [isLoading, setLoading] = useState(false);
  const [showPass, setshowPass] = useState(true);
  const eyeIcon = showPass ? 'eye' : 'eye-off-outline';
  const fetchLoginDetails = useSelector(state => state.users);

  const loginAPI = async () => {
    alert(fcmToken);
    if (!email) {
      alert('Please enter email');
      return false;
    }
    try {
      const response = await fetch(otherLinks.login, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email: email,
          password: password,
          token: fcmToken,
        }),
      });
      const json = await response.json();
      //console.log('return Content: ',json.data);
      //console.log('return Email: ',json.data.email);
      changeloginStatus(updateUserAuth({data: json.data}));
      //console.log('IN Redux STATE: ', fetchLoginDetails);
    } catch (error) {
      console.error(error);
    } finally {
      //setLoading(false);
    }
  };

  const [email, setEmail] = useState('pp2@gmail.com');
  const [password, setPassword] = useState('123456');
  // const [email, setEmail] = useState('');
  // const [password, setPassword] = useState('');
  return (
    <ScrollView style={styles.container}>
      <View style={styles.wrapper}>
        <Text>{/*fetchLoginDetails*/}</Text>
        <Text style={styles.Headingtext}>Login</Text>
        <View style={styles.formWrapper}>
          <Input
            placeholder="email"
            value={email}
            leftIcon={{type: 'font-awesome', name: 'envelope'}}
            onChangeText={email => setEmail(email)}
            label="Email"
          />
          <Input
            placeholder="Password"
            value={password}
            leftIcon={{type: 'font-awesome', name: 'lock'}}
            rightIcon={{type: 'ionicon', name: eyeIcon, onPress: showPassword}}
            onChangeText={password => setPassword(password)}
            secureTextEntry={showPass}
            label="Password"
          />
          <View style={[styles.centerContent, styles.directionRow]}>
            <TouchableOpacity onPress={() => loginAPI()}>
              <Text style={styles.buttonDesign}>Log in</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.VerticalSpace} />
        <View style={styles.VerticalSpace} />
        <View style={styles.VerticalSpace} />
        <View style={[styles.directionRow, styles.centerContent]}>
          <Text>Don't have an account?</Text>
          <TouchableOpacity onPress={() => navigation.navigate('Register')}>
            <Text style={styles.link}> Register</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

export default Login;
