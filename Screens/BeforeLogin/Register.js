import React, {useState} from 'react';
import {Text, View, ScrollView, TouchableOpacity} from 'react-native';
import {Input} from 'react-native-elements/dist/input/Input';
import styles from '../../constants/styles';
import {otherLinks} from '../../constants/Links';
import {Alert} from 'react-native';

const Register = ({navigation}) => {

  const [name, setname] = useState();
  const [email, setEmail] = useState();
  const [phoneNumber, setPhone] = useState();
  const [password, setPassword] = useState();


  const registerAPI = async () => {
    if (!email) {
      Alert.alert('Error', 'Please enter email');
      return false;
    }
    try {
      const response = await fetch(otherLinks.register, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          name: name,
          email: email,
          password: password,
          phone: phoneNumber,
        }),
      });
      const json = await response.json();
      console.log('After registration data', json);
      if (json.message == 'The user has been added successfully.') {
        alert('You have been registererd successfully, Please login!');
      }
      if (json == 'The given email already exists.') {
        alert(json);
      }
      //changeloginStatus(updateUserAuth({email: email}));
    } catch (error) {
      console.error(error);
    } finally {
      //setLoading(false);
    }
  };

  return (
    <ScrollView>
      <View style={styles.wrapper}>
        <Text style={styles.Headingtext}>Register</Text>
        <View style={styles.formWrapper}>
          <Input
            placeholder="Full Name"
            value={name}
            leftIcon={{type: 'font-awesome', name: 'user'}}
            style={styles}
            onChangeText={name => setname(name)}
          />
          <Input
            placeholder="email"
            value={email}
            leftIcon={{type: 'font-awesome', name: 'envelope'}}
            style={styles}
            onChangeText={email => setEmail(email)}
          />
          <Input
            placeholder="phone"
            value={phoneNumber}
            leftIcon={{type: 'font-awesome', name: 'phone'}}
            style={styles}
            onChangeText={phoneNumber => setPhone(phoneNumber)}
          />
          <Input
            placeholder="Password"
            value={password}
            leftIcon={{type: 'font-awesome', name: 'lock'}}
            style={styles}
            onChangeText={password => setPassword(password)}
            secureTextEntry={true}
          />
          <View style={[styles.centerContent, styles.directionRow]}>
            <TouchableOpacity onPress={() => registerAPI()}>
              <Text style={styles.buttonDesign}>Register</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.VerticalSpace} />
        <View style={styles.VerticalSpace} />
        <View style={[styles.directionRow, styles.centerContent]}>
          <Text>Already have an account?</Text>
          <TouchableOpacity onPress={() => navigation.navigate('Login')}>
            <Text style={styles.link}> Login</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

export default Register;
