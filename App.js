// In App.js in a new project

import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {createStore, combineReducers} from 'redux';
import {Provider, useSelector} from 'react-redux';
import messaging from '@react-native-firebase/messaging';

import Login from './Screens/BeforeLogin/Login';
import Register from './Screens/BeforeLogin/Register';

import Createtask from './Screens/AfterLogin/Createtask';
import Tasklist from './Screens/AfterLogin/Tasklist';
import UnassignedTask from './Screens/AfterLogin/UnassignedTask';
import Singletask from './Screens/AfterLogin/Singletask';
import Myaccount from './Screens/AfterLogin/Myaccount';

import userReducer from './store/reducers/usersReducer';
import {Alert} from 'react-native';

const appReducer = combineReducers({
  users: userReducer,
});

const rootReducer = (state, action) => {
  // when a logout action is dispatched it will reset redux state
  if (action.type === 'LOGOUT_USER') {
    state = undefined;
  }

  return appReducer(state, action);
};

const store = createStore(rootReducer);

const TasksInners = createNativeStackNavigator();
const TasksInnersScreens = () => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Tasklist"
        component={Tasklist}
        options={{headerShown: false}}
      />
      <Stack.Screen name="Singletask" component={Singletask} />
    </Stack.Navigator>
  );
};

const Tab = createBottomTabNavigator();

const AfterLoginTabs = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;

          if (route.name === 'Create Task') {
            iconName = 'add-circle';
          }
          if (route.name === 'Task List') {
            iconName = 'ios-list';
          }
          if (route.name === 'Unassigned task') {
            iconName = 'link';
          }
          if (route.name === 'My Account') {
            iconName = 'person';
          }

          // You can return any component that you like here!
          return <Ionicons name={iconName} size={size} color={color} />;
        },
        tabBarActiveTintColor: 'tomato',
        tabBarInactiveTintColor: 'gray',
      })}>
      <Tab.Screen
        name="Create Task"
        component={Createtask}
        options={{headerShown: false}}
      />
      <Tab.Screen
        name="Task List"
        component={TasksInnersScreens}
        options={{headerShown: false}}
      />
      <Tab.Screen
        name="Unassigned task"
        component={UnassignedTask}
        options={{headerShown: false}}
      />
      <Tab.Screen
        name="My Account"
        component={Myaccount}
        options={{headerShown: false}}
      />
    </Tab.Navigator>
  );
};

const Stack = createNativeStackNavigator();
const BeforeLoginScreens = () => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

const LoginCheck = () => {
  const loggedIn = useSelector(state => state.users.email);
  const userData = useSelector(state => state.users);
  console.log('App.js Redux Data: ', userData);
  return loggedIn.length > 0 ? <AfterLoginTabs /> : <BeforeLoginScreens />;
};

const requestUserPermission = async () => {
  const Token = await messaging().getToken();
  alert(Token);
  console.log('token1: ', Token);
  const unsubscribe = messaging().onMessage(async remoteMessage => {
    Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
  });

  return unsubscribe;
};

const App = () => {
  React.useEffect(() => {
    //requestUserPermission();
  }, []);

  return (
    <Provider store={store}>
      <SafeAreaProvider>
        <NavigationContainer>
          <LoginCheck />
        </NavigationContainer>
      </SafeAreaProvider>
    </Provider>
  );
};

export default App;
